package com.crio.qkart.backend;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableScheduling
public class UserApplication {

  @GetMapping("/")
  public String home() {
    return "Welcome to ZCommerce - User";
  }

	public static void main(String[] args) {
		SpringApplication.run(UserApplication.class, args);
	}

  @Configuration
  public class ModelMapperConfig {
    @Bean
    public ModelMapper modelMapper() {
      return new ModelMapper();
    }
  }
}
