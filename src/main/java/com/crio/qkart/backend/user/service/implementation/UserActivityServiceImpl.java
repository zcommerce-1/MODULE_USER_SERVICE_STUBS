package com.crio.qkart.backend.user.service.implementation;

import com.crio.qkart.backend.user.config.kafka.KafkaTopicConfig;
import com.crio.qkart.backend.user.dao.clickstream.activity.UserActivityDao;
import com.crio.qkart.backend.user.model.Activity;
import com.crio.qkart.backend.user.service.UserActivityService;
import com.crio.qkart.backend.user.utils.kafka.KafkaUtil;
import com.crio.qkart.backend.user.utils.kafka.model.ActivityMessageEvent;
import com.crio.qkart.backend.user.utils.kafka.model.ConsumerEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserActivityServiceImpl implements UserActivityService {
    private final UserActivityDao userActivityDao;
    private final KafkaUtil kafkaUtil;
    private final KafkaTopicConfig kafkaTopicConfig;

    @Override
    public void addActivitySync(Activity activity, String userId) {
        activity.setUserId(userId);
        userActivityDao.addActivity(activity);
    }

    @Override
    public void addActivityAsync(Activity activity, String userId) {
        activity.setUserId(userId);
        sendActivityCreationEvent(activity);
    }

    private void sendActivityCreationEvent(Activity activity) {
        ConsumerEvent event = ActivityMessageEvent.builder()
                .userId(activity.getUserId())
                .activity(activity)
                .build();

        kafkaUtil.sendEvent(event, kafkaTopicConfig.getClickStreamTopic());
        log.info("event: sendActivityCreationEvent, message: sent event to clickstream topic userId : {}",
                activity.getUserId());
    }
}
