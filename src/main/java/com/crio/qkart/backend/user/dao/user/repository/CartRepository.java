package com.crio.qkart.backend.user.dao.user.repository;

import com.crio.qkart.backend.user.dao.user.entity.CartEntity;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<CartEntity, Long> {

  Optional<CartEntity> findByUserId(String userId);

}
