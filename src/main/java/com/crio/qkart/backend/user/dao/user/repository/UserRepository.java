package com.crio.qkart.backend.user.dao.user.repository;

import com.crio.qkart.backend.user.dao.user.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

}
