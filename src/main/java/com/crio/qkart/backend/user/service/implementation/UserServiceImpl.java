package com.crio.qkart.backend.user.service.implementation;

import com.crio.qkart.backend.user.common.exceptions.EntityDoesNotExistException;
import com.crio.qkart.backend.user.dao.user.UserDao;
import com.crio.qkart.backend.user.model.User;
import com.crio.qkart.backend.user.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Log4j2
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

  private final UserDao userDao;

  @Override
  public User getUserDetails(Long userId) throws EntityDoesNotExistException {
    String logPrefix = "event: getUserDetails, message: ";
    Optional<User> userOptional = userDao.findByUserId(userId);
    if (userOptional.isPresent()) {
      User user = userOptional.get();
      log.info(logPrefix + "user details fetched for userId : {}", userId);
      return user;
    } else {
      log.info(logPrefix + "user details not found for the userId : {}", userId);
      throw new EntityDoesNotExistException(
          String.format("%s user details not found for the userId : %s", logPrefix, userId));
    }
  }

  @Override
  public User addUser(User user) {
    String logPrefix = "event: addUser, message: ";
    User addedUser = userDao.addUser(user);
    log.info(logPrefix + "new user got added, id: {}", addedUser.getId());
    return addedUser;
  }

  @Override
  public User updateUser(Long userId, User user) throws EntityDoesNotExistException {
    String logPrefix = "event: updateUser, message: ";
    User updateUser = userDao.updateUser(userId, user);
    log.info(logPrefix + "User has been updated, id: {}", updateUser.getId());
    return updateUser;
  }

  @Override
  public void deleteUser(Long userId) {
    String logPrefix = "event: deleteUser, message: ";
    log.info(logPrefix + "User is being deleted, id: {}", userId);
    userDao.deleteByUserId(userId);
  }
}
