package com.crio.qkart.backend.user.dao.clickstream.activity;

import com.crio.qkart.backend.user.model.Activity;

import java.time.ZonedDateTime;

public interface UserActivityDao {
    Activity addActivity(Activity activity);

    void deleteActivityOlderThan(ZonedDateTime dateTime);
}
