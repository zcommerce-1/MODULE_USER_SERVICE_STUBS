package com.crio.qkart.backend.user.dao.user.implementation;

import com.crio.qkart.backend.user.common.exceptions.EntityDoesNotExistException;
import com.crio.qkart.backend.user.dao.user.CartDao;
import com.crio.qkart.backend.user.dao.user.entity.CartEntity;
import com.crio.qkart.backend.user.dao.user.repository.CartRepository;
import com.crio.qkart.backend.user.model.Cart;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@Log4j2
@RequiredArgsConstructor
public class CartDaoImpl implements CartDao {

  private final CartRepository cartRepository;

  private final ModelMapper modelMapper;

  @Override
  public Optional<Cart> findByUserId(String userId) {
    Optional<CartEntity> cartEntityOptional = cartRepository.findByUserId(userId);

    // check if cart details is present
    if (cartEntityOptional.isPresent()) {
      Cart cart = modelMapper.map(cartEntityOptional.get(), Cart.class);
      return Optional.of(cart);
    }

    // returning empty optional as cart details is not present
    return Optional.empty();
  }

  @Override
  public Cart createCart(Cart cart) {
    CartEntity cartEntity = modelMapper.map(cart, CartEntity.class);

    // inserting cart entity into cart table
    CartEntity cartEntity1 = cartRepository.save(cartEntity);
    return modelMapper.map(cartEntity1, Cart.class);
  }

  @Override
  public Cart updateCart(Long cartId, Cart cart) throws EntityDoesNotExistException {
    String logPrefix = "event: updateCart, message: ";

    // fetching the existing cart entity for the cartId
    Optional<CartEntity> existingCartEntityOptional = cartRepository.findById(cartId);

    if (!existingCartEntityOptional.isPresent()) {
      log.info(logPrefix + "cart details not found for the cartId : {}", cartId);
      throw new EntityDoesNotExistException(
          String.format("%s update failed as cart details not found for the cartId : %s",
              logPrefix, cartId));
    }

    CartEntity cartEntity = existingCartEntityOptional.get();
    cartEntity.setItemQuantityList(cart.getItemQuantityList());
    CartEntity updateCartEntity = cartRepository.save(cartEntity);
    return modelMapper.map(updateCartEntity, Cart.class);
  }

  @Override
  public void deleteCart(Long cartId) {
    cartRepository.deleteById(cartId);
  }
}
