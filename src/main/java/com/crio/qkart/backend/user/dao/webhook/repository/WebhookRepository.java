package com.crio.qkart.backend.user.dao.webhook.repository;

import com.crio.qkart.backend.user.dao.webhook.entity.WebhookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.Optional;

@Repository
public interface WebhookRepository extends JpaRepository<WebhookEntity, Long> {
    Optional<WebhookEntity> findByUserIdAndCallbackUrl(String userId, String callbackUrl);

    void deleteByCreatedAtLessThanEqual(ZonedDateTime thresholdDate);
}
