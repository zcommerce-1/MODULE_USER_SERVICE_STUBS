package com.crio.qkart.backend.user.kafka.consumer;

import com.crio.qkart.backend.user.dao.clickstream.activity.UserActivityDao;
import com.crio.qkart.backend.user.utils.kafka.model.ActivityMessageEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Log4j2
@RequiredArgsConstructor
public class ActivityConsumer {
    private final UserActivityDao userActivityDao;
    private final ObjectMapper objectMapper;

    private final String LISTENER_LOG_PREFIX = "event: ActivityConsumer.listen, message: ";

    @KafkaListener(topics = "#{kafkaTopicConfig.clickStreamTopic}",
            groupId = "#{kafkaTopicConfig.clickStreamConsumerGroup}",
            containerFactory = "kafkaListenerContainerFactory")
    public void listen(String message) {
        try {
            log.info(LISTENER_LOG_PREFIX + "message consumed from Kafka: {}", message);
            ActivityMessageEvent event = objectMapper.readValue(message, ActivityMessageEvent.class);
            userActivityDao.addActivity(event.getActivity());
        } catch (JsonProcessingException ex) {
            log.error(LISTENER_LOG_PREFIX + "JsonProcessingException thrown. Consumed message: {}",
                    message);
        } catch (InvalidDataAccessResourceUsageException ex) {
            log.error(LISTENER_LOG_PREFIX + "InvalidDataAccessResourceUsageException thrown.");
            throw ex; // Re-throw the exception to trigger retry
        } catch (Exception ex) {
            log.error(LISTENER_LOG_PREFIX + "Exception thrown. msg={}", ex.getMessage());
        }
    }
}
