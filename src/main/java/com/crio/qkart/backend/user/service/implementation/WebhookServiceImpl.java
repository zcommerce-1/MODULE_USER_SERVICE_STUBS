package com.crio.qkart.backend.user.service.implementation;

import com.crio.qkart.backend.user.clients.NetworkClient;
import com.crio.qkart.backend.user.common.exceptions.InvalidWebhookCallbackException;
import com.crio.qkart.backend.user.common.exceptions.WebhookExistsException;
import com.crio.qkart.backend.user.dao.webhook.WebhookDao;
import com.crio.qkart.backend.user.model.Webhook;
import com.crio.qkart.backend.user.model.WebhookWithLog;
import com.crio.qkart.backend.user.model.enums.WebhookTrigger;
import com.crio.qkart.backend.user.service.WebhookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Headers;
import okhttp3.Response;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service
@Slf4j
public class WebhookServiceImpl implements WebhookService {
    private final NetworkClient client;
    private final WebhookDao webhookDao;

    @Override
    public Webhook createWebhook(Webhook webhook)
            throws InvalidWebhookCallbackException, WebhookExistsException {
        // Test the callback url
        try(Response response = client.head(webhook.getCallbackUrl(), new Headers.Builder().build())) {
            if (response.code() == 200) {
                // check if webhook already registered
                Webhook existingWebhook = webhookDao.getWebhooks(webhook.getUserId(), webhook.getCallbackUrl());

                if (existingWebhook != null) {
                    List<WebhookTrigger> newTriggers = new ArrayList<>(webhook.getTriggers());

                    newTriggers.removeIf(existingWebhook.getTriggers()::contains);

                    if (!newTriggers.isEmpty()) {
                        // update
                        webhook.setId(existingWebhook.getId());
                        webhook.setCreatedAt(existingWebhook.getCreatedAt());
                    }
                    else {
                        throw new WebhookExistsException("A webhook with the given list of triggers already exists");
                    }
                }

                return webhookDao.addWebhook(webhook);
            }
            else {
                throw new InvalidWebhookCallbackException("Callback URL validation failed: Non-200 status returned");
            }
        } catch (IOException ex) {
            log.error("event: createWebhook, msg: callback URL verification failed", ex);
            throw new InvalidWebhookCallbackException("Callback URL validation failed");
        }
    }

    @Override
    public List<WebhookWithLog> getWebhookLogs(String userId) {
        return webhookDao.getWebhookLogs(userId);
    }
}
