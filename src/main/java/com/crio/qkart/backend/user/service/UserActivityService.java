package com.crio.qkart.backend.user.service;

import com.crio.qkart.backend.user.model.Activity;

public interface UserActivityService {
    /**
     * Add activity synchronously
     *
     * @param activity of type Activity
     * @param userId of type String
     */
    void addActivitySync(Activity activity, String userId);

    /**
     * Add activity asynchronously
     *
     * @param activity of type Activity
     * @param userId of type String
     */
    void addActivityAsync(Activity activity, String userId);
}
