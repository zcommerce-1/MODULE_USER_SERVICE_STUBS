package com.crio.qkart.backend.user.dao.clickstream.activity.repository;

import com.crio.qkart.backend.user.dao.clickstream.activity.entity.ActivityEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;

@Repository
public interface ActivityRepository extends JpaRepository<ActivityEntity, Long> {
    void deleteByCreatedAtLessThanEqual(ZonedDateTime thresholdDate);
}
