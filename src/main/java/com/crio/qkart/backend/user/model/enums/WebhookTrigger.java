package com.crio.qkart.backend.user.model.enums;

public enum WebhookTrigger {
    NEW_ORDER,
    REMOVE_CART_ITEM,
    ADD_CART_ITEM
}
