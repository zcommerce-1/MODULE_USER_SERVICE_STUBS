package com.crio.qkart.backend.user.controller;

import com.crio.qkart.backend.user.common.api.ApiResponse;
import com.crio.qkart.backend.user.common.api.ApiResponseStatus;
import com.crio.qkart.backend.user.common.exceptions.EntityDoesNotExistException;
import com.crio.qkart.backend.user.model.Cart;
import com.crio.qkart.backend.user.service.CartService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.metrics.annotation.Timed;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Timed
@RestController
@RequiredArgsConstructor
@RequestMapping(CartController.CART_API_PREFIX)
public class CartController {

  public static final String CART_API_PREFIX = "api/v1/cart";
  private static final String ID_ENDPOINT = "/{id}";
  private final CartService cartService;


  /**
   * URL: /api/v1/cart
   * Description: Get cart details of the user.
   * Method: GET
   *
   * @return cart details
   */
  @GetMapping
  @Timed
  public ResponseEntity<ApiResponse<Cart>> getCartDetails(
      @RequestParam(value = "userId", required = true) String userId) {
    ApiResponse<Cart> response = new ApiResponse<>();
    try {
      Cart cart = cartService.getCartDetails(userId);
      response.setData(cart);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(
          String.format("cart details not found for the given userId : %s", userId));
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
  }

  /**
   * URL: /api/v1/cart
   * Description: Create cart
   * Method: POST
   *
   * @param cart of type Cart
   * @return Created Cart details
   */
  @PostMapping
  @Timed
  public ResponseEntity<ApiResponse<Cart>> createCart(@RequestBody @Valid Cart cart) {
    // initialising userId fetched from the principal to the request object
    // cart.setUserId(principal.getName());

    // Creating cart for the user
    Cart createdCart = cartService.createCart(cart);

    ApiResponse<Cart> response = new ApiResponse<>();
    response.setData(createdCart);
    response.setStatus(ApiResponseStatus.SUCCESS);
    return ResponseEntity.status(HttpStatus.OK).body(response);
  }

  /**
   * URL: /api/v1/cart/{id}
   * Description: Update cart details
   * Method: PUT
   *
   * @param cart of type Cart
   * @return updated cart details
   */
  @PutMapping(ID_ENDPOINT)
  @Timed
  public ResponseEntity<ApiResponse<Cart>> updateOrderStatus(@PathVariable("id") Long cartId,
      @RequestBody @Valid Cart cart) {
    ApiResponse<Cart> response = new ApiResponse<>();
    try {
      Cart updatedCart = cartService.updateCart(cartId, cart);
      response.setData(updatedCart);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(
          String.format("Update cart failed as cart Details not found for the given cartId : %s",
              cartId));
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
  }

  /**
   * URL: /api/v1/cart/{id}
   * Description: Delete cart of given cart id
   * Method: DELETE
   */
  @DeleteMapping(ID_ENDPOINT)
  @Timed
  public ResponseEntity<ApiResponse<String>> deleteOrder(@PathVariable("id") Long cartId) {
    cartService.deleteCart(cartId);
    ApiResponse<String> response = new ApiResponse<>();
    response.setStatus(ApiResponseStatus.SUCCESS);
    response.setMessage("Cart deleted");
    return ResponseEntity.status(HttpStatus.OK).body(response);
  }

}
