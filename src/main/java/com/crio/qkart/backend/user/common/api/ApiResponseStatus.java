package com.crio.qkart.backend.user.common.api;

public enum ApiResponseStatus {

  SUCCESS,

  FAILED
}
