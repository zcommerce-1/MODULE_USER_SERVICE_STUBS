package com.crio.qkart.backend.user.model.enums;

public enum UserType {

  NORMAL,

  SELLER,

  ADMIN
}
