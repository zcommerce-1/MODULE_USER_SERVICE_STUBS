package com.crio.qkart.backend.user.tasks;

import com.crio.qkart.backend.user.dao.clickstream.activity.UserActivityDao;
import com.crio.qkart.backend.user.dao.webhook.WebhookDao;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;

@Component
@Slf4j
@RequiredArgsConstructor
public class CleanupTask {
    private final WebhookDao webhookDao;
    private final UserActivityDao userActivityDao;

    // Schedule a task for every 24 hours
    @Scheduled(fixedRate = (24*60*60*1000))
    public void cleanupWebhooks() {
        log.info("Webhook cleanup job starting");
        try {
            // Delete webhooks older than 24 hours from now
            ZonedDateTime deleteBefore = ZonedDateTime.now().minusHours(24L);
            webhookDao.deleteWebhookOlderThan(deleteBefore);
            log.info("Webhook cleanup job completed");
        } catch (Exception ex) {
            log.error("Webhook cleanup job threw an exception", ex);
        }
    }

    @Scheduled(fixedRate = (24*60*60*1000))
    public void cleanupClickStreamLogs() {
        log.info("Click stream cleanup job starting");
        try {
            // Delete webhooks older than a week from now
            ZonedDateTime deleteBefore = ZonedDateTime.now().minusWeeks(1L);
            userActivityDao.deleteActivityOlderThan(deleteBefore);
            log.info("Click stream cleanup job completed");
        } catch (Exception ex) {
            log.error("Click stream cleanup job threw an exception", ex);
        }
    }
}
