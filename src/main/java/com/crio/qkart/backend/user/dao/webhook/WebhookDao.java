package com.crio.qkart.backend.user.dao.webhook;

import com.crio.qkart.backend.user.model.Webhook;
import com.crio.qkart.backend.user.model.WebhookWithLog;

import java.time.ZonedDateTime;
import java.util.List;

public interface WebhookDao {
    Webhook addWebhook(Webhook webhook);
    Webhook getWebhooks(String userId, String callbackUrl);
    List<WebhookWithLog> getWebhookLogs(String userId);

    void deleteWebhookOlderThan(ZonedDateTime dateTime);
}
