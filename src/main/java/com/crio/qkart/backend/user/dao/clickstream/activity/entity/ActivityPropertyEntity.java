package com.crio.qkart.backend.user.dao.clickstream.activity.entity;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(
        name = "activity_event_attributes"
)
public class ActivityPropertyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Column(name = "property_key")
    private String key;

    @NonNull
    @Column(name = "property_value")
    private String value;
}
