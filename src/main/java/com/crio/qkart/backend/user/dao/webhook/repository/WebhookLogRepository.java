package com.crio.qkart.backend.user.dao.webhook.repository;

import com.crio.qkart.backend.user.dao.webhook.entity.WebhookLogEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WebhookLogRepository extends JpaRepository<WebhookLogEntity, Long> {
    Optional<List<WebhookLogEntity>> findByWebhookUserIdOrderBySentAtDesc(String userId);
}
