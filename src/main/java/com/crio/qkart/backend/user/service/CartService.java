package com.crio.qkart.backend.user.service;


import com.crio.qkart.backend.user.common.exceptions.EntityDoesNotExistException;
import com.crio.qkart.backend.user.model.Cart;

public interface CartService {

  /**
   * Get cart details for the given userId
   *
   * @param userId of type string
   * @return cart details if present
   * @throws EntityDoesNotExistException when cart details is not found against given userId
   */
  Cart getCartDetails(String userId) throws EntityDoesNotExistException;

  /**
   * Create cart
   *
   * @param cart of type Cart
   * @return cart details of the actual cart created
   */
  Cart createCart(Cart cart);

  /**
   * Update cart
   *
   * @param cartId of type long
   * @param cart of type Cart to update
   * @return updated cart details
   */
  Cart updateCart(Long cartId, Cart cart) throws EntityDoesNotExistException;

  /**
   * Delete cart details
   *
   * @param cartId of type Cart
   */
  void deleteCart(Long cartId);
}
