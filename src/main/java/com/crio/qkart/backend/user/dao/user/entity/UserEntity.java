package com.crio.qkart.backend.user.dao.user.entity;

import com.crio.qkart.backend.user.dao.BaseEntity;
import com.crio.qkart.backend.user.model.enums.UserType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import java.util.Objects;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(
    name = "user",
    uniqueConstraints = {
        @UniqueConstraint(columnNames = "email"),
    },
    indexes = @Index(name = "IDX_email", columnList = "email")
)
public class UserEntity extends BaseEntity {

  @Column(nullable = false)
  private String email;

  @Column(nullable = false)
  private String fullName;

  @Column(nullable = false)
  private UserType type;

  @Lob
  private String address;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
      return false;
    }
    UserEntity that = (UserEntity) o;
    return getId() != null && Objects.equals(getId(), that.getId());
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
