package com.crio.qkart.backend.user.service.implementation;

import com.crio.qkart.backend.user.common.exceptions.EntityDoesNotExistException;
import com.crio.qkart.backend.user.dao.user.CartDao;
import com.crio.qkart.backend.user.model.Cart;
import com.crio.qkart.backend.user.service.CartService;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@Log4j2
@RequiredArgsConstructor
public class CartServiceImpl implements CartService {

  private final CartDao cartDao;

  @Override
  public Cart getCartDetails(String userId) throws EntityDoesNotExistException {
    String logPrefix = "event: getCartDetails, message: ";
    Optional<Cart> cartOptional = cartDao.findByUserId(userId);
    if (cartOptional.isPresent()) {
      return cartOptional.get();
    } else {
      log.info(logPrefix + "cart details not found for the userId : {}", userId);
      throw new EntityDoesNotExistException(
          String.format("%s cart details not found for the userId : %s", logPrefix,userId));
    }
  }

  @Override
  public Cart createCart(Cart cart) {
    return cartDao.createCart(cart);
  }

  @Override
  public Cart updateCart(Long cartId, Cart cart) throws EntityDoesNotExistException {
    return cartDao.updateCart(cartId,cart);
  }

  @Override
  public void deleteCart(Long cartId) {
    cartDao.deleteCart(cartId);
  }
}
