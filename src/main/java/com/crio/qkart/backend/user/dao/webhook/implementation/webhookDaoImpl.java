package com.crio.qkart.backend.user.dao.webhook.implementation;

import com.crio.qkart.backend.user.dao.webhook.WebhookDao;
import com.crio.qkart.backend.user.dao.webhook.entity.WebhookEntity;
import com.crio.qkart.backend.user.dao.webhook.entity.WebhookLogEntity;
import com.crio.qkart.backend.user.dao.webhook.repository.WebhookLogRepository;
import com.crio.qkart.backend.user.dao.webhook.repository.WebhookRepository;
import com.crio.qkart.backend.user.model.Webhook;
import com.crio.qkart.backend.user.model.WebhookLog;
import com.crio.qkart.backend.user.model.WebhookWithLog;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.*;

@Service
@Log4j2
@RequiredArgsConstructor
public class webhookDaoImpl implements WebhookDao {
    private final WebhookRepository webhookRepository;
    private final WebhookLogRepository webhookLogRepository;
    private final ModelMapper modelMapper;
    @Override
    @Transactional("webhookTransactionManager")
    public Webhook addWebhook(Webhook webhook) {
        WebhookEntity entity = modelMapper.map(webhook, WebhookEntity.class);

        entity = webhookRepository.save(entity);

        return modelMapper.map(entity, Webhook.class);
    }

    @Override
    @Transactional("webhookTransactionManager")
    public Webhook getWebhooks(String userId, String callbackUrl) {
        Optional<WebhookEntity> entity = webhookRepository.findByUserIdAndCallbackUrl(userId, callbackUrl);

        return entity
                .map(e -> modelMapper.map(e, Webhook.class))
                .orElse(null);
    }

    @Override
    @Transactional("webhookTransactionManager")
    public List<WebhookWithLog> getWebhookLogs(String userId) {
        Optional<List<WebhookLogEntity>> entityList = webhookLogRepository.findByWebhookUserIdOrderBySentAtDesc(userId);
        Map<Long, WebhookWithLog> webhookListMap = new HashMap<>();

        for (WebhookLogEntity entity : entityList.orElse(List.of())) {
            Webhook webhook = modelMapper.map(entity.getWebhook(), Webhook.class);
            WebhookLog log = modelMapper.map(entity, WebhookLog.class);

            Long id = entity.getWebhook().getId();
            if (!webhookListMap.containsKey(id)) {
                WebhookWithLog withLog = new WebhookWithLog(webhook, new ArrayList<>());
                webhookListMap.put(id, withLog);
            }
            webhookListMap.get(id).getLogs().add(log);
        }

        return new ArrayList<>(webhookListMap.values());
    }

    @Override
    @Transactional("webhookTransactionManager")
    public void deleteWebhookOlderThan(ZonedDateTime dateTime) {
        webhookRepository.deleteByCreatedAtLessThanEqual(dateTime);
    }
}
