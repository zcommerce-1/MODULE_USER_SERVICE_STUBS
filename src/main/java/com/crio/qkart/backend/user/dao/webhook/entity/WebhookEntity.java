package com.crio.qkart.backend.user.dao.webhook.entity;

import com.crio.qkart.backend.user.dao.BaseEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@ToString
@SuperBuilder
@RequiredArgsConstructor
@Entity
@Table(
        name = "webhook",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "id")
        }
)
public class WebhookEntity extends BaseEntity {
    private String userId;
    private String apiKey;
    private String callbackUrl;

    @ElementCollection(targetClass = String.class, fetch = FetchType.EAGER)
    @CollectionTable(
            name = "webhook_triggers",
            joinColumns = @JoinColumn(name = "wh_id"),
            indexes = {@Index(name = "wh_trigger_idx", columnList = "wh_trigger")})
    @Column(name = "wh_trigger", nullable = false)
    private List<String> triggers;
}

