package com.crio.qkart.backend.user.common.exceptions;

public class EntityDoesNotExistException extends Exception {

  private static final long serialVersionUID = 1L;

  public EntityDoesNotExistException(String message) {
    super(message);
  }
  
}

