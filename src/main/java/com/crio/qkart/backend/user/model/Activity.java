package com.crio.qkart.backend.user.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.time.ZonedDateTime;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Activity extends BaseModel{
    private String pageUrl;
    private String referer;
    private String userId;
    private String eventType;
    private String element_id;
    private String element_class;
    private ZonedDateTime eventTime;
    private List<ActivityProperty> attributes;
}