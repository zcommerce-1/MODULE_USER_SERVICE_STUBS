package com.crio.qkart.backend.user.common.exceptions;

public class InvalidWebhookCallbackException extends Exception {
    private static final long serialVersionUID = 1L;

    public InvalidWebhookCallbackException(String message) {
        super(message);
    }
}
