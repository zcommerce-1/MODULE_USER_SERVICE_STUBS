package com.crio.qkart.backend.user.model;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode(callSuper = true)
public class Cart extends BaseModel{

  private String userId;

  @NotNull
  @NotEmpty
  private List<ItemQuantity> itemQuantityList;
}
