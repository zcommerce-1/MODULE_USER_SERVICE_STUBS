package com.crio.qkart.backend.user.dao.clickstream.activity.implementation;

import com.crio.qkart.backend.user.dao.clickstream.activity.UserActivityDao;
import com.crio.qkart.backend.user.dao.clickstream.activity.entity.ActivityEntity;
import com.crio.qkart.backend.user.dao.clickstream.activity.repository.ActivityRepository;
import com.crio.qkart.backend.user.model.Activity;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;

@Service
@Log4j2
@RequiredArgsConstructor
public class UserActivityDaoImpl implements UserActivityDao {
    private final ActivityRepository activityRepository;
    private final ModelMapper modelMapper;

    @Override
    @Transactional("writeLimitedTransactionManager")
    public Activity addActivity(Activity activity) {
        ActivityEntity entity = modelMapper.map(activity, ActivityEntity.class);

        entity = activityRepository.save(entity);

        return modelMapper.map(entity, Activity.class);
    }

    @Override
    @Transactional("writeLimitedTransactionManager")
    public void deleteActivityOlderThan(ZonedDateTime dateTime) {
        activityRepository.deleteByCreatedAtLessThanEqual(dateTime);
    }
}
