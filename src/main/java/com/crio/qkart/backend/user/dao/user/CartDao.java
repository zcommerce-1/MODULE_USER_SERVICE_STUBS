package com.crio.qkart.backend.user.dao.user;

import com.crio.qkart.backend.user.common.exceptions.EntityDoesNotExistException;
import com.crio.qkart.backend.user.model.Cart;
import java.util.Optional;

public interface CartDao {

  Optional<Cart> findByUserId(String userId);

  Cart createCart(Cart cart);

  Cart updateCart(Long cartId, Cart cart) throws EntityDoesNotExistException;

  void deleteCart(Long cartId);
}
