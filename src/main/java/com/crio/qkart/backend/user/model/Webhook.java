package com.crio.qkart.backend.user.model;

import com.crio.qkart.backend.user.model.enums.WebhookTrigger;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Webhook extends BaseModel {
    private String callbackUrl;
    private List<WebhookTrigger> triggers;
    private String apiKey;
    private String userId;
}
