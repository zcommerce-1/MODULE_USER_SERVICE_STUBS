package com.crio.qkart.backend.user.common.exceptions;

public class WebhookExistsException extends Exception {
    private static final long serialVersionUID = 1L;

    public WebhookExistsException(String message) {
        super(message);
    }
}
