package com.crio.qkart.backend.user.service;

import com.crio.qkart.backend.user.common.exceptions.EntityDoesNotExistException;
import com.crio.qkart.backend.user.model.Activity;
import com.crio.qkart.backend.user.model.User;

public interface UserService {

  /**
   * Get user details for the given user Id
   *
   * @param userId of type Long
   * @return user details if present
   * @throws EntityDoesNotExistException when user details is not found against given userId
   */
  User getUserDetails(Long userId) throws EntityDoesNotExistException;

  /**
   * Add or create user
   *
   * @param user of type Item
   * @return user details of the actual user created
   */
  User addUser(User user);

  /**
   * Update user
   *
   * @param userId of type long
   * @param user of type User to update
   * @return updated user details
   */
  User updateUser(Long userId, User user) throws EntityDoesNotExistException;

  /**
   * Delete user details
   *
   * @param userId of type Long
   */
  void deleteUser(Long userId);
}
