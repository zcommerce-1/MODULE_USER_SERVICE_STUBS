package com.crio.qkart.backend.user.model;

import com.crio.qkart.backend.user.model.enums.WebhookTrigger;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.time.ZonedDateTime;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class WebhookLog {
    private WebhookTrigger trigger;
    private ZonedDateTime sentAt;
    private int statusCode;
    private String comment;
}
