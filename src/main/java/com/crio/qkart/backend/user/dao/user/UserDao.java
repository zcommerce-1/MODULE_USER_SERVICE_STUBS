package com.crio.qkart.backend.user.dao.user;

import com.crio.qkart.backend.user.common.exceptions.EntityDoesNotExistException;
import com.crio.qkart.backend.user.model.User;

import java.util.Optional;

public interface UserDao {
  Optional<User> findByUserId(Long userId);

  User addUser(User user);

  User updateUser(Long userId, User user) throws EntityDoesNotExistException;

  void deleteByUserId(Long userId);
}
