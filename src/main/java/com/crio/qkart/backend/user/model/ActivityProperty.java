package com.crio.qkart.backend.user.model;

import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class ActivityProperty {
    private String key;
    private String value;
}
