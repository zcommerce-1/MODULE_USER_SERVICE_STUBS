package com.crio.qkart.backend.user.controller;

import com.crio.qkart.backend.user.common.api.ApiResponse;
import com.crio.qkart.backend.user.common.api.ApiResponseStatus;
import com.crio.qkart.backend.user.common.exceptions.InvalidWebhookCallbackException;
import com.crio.qkart.backend.user.common.exceptions.WebhookExistsException;
import com.crio.qkart.backend.user.model.Webhook;
import com.crio.qkart.backend.user.model.WebhookWithLog;
import com.crio.qkart.backend.user.model.enums.WebhookTrigger;
import com.crio.qkart.backend.user.service.WebhookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.metrics.annotation.Timed;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@Slf4j
@Timed
@RestController
@RequiredArgsConstructor
@RequestMapping(WebhookController.API_PREFIX)
public class WebhookController {
    public static final String API_PREFIX = "api/v1/webhook";
    public static final String API_LOGS = "/log";
    public static final String API_TRIGGERS = "/trigger";

    private final WebhookService webhookService;

    @PostMapping
    @Timed
    public ResponseEntity<ApiResponse<Webhook>> createWebhook(
            @RequestBody Webhook webhookRequest,
            Principal principal) {
        String userId = principal.getName();
        webhookRequest.setUserId(userId);
        ApiResponse<Webhook> response = new ApiResponse<>();
        HttpStatusCode statusCode = HttpStatus.OK;

        try {
            Webhook webhookResp = webhookService.createWebhook(webhookRequest);

            response.setStatus(ApiResponseStatus.SUCCESS);
            response.setMessage("Webhook registered successfully");
            response.setData(webhookResp);
        } catch (InvalidWebhookCallbackException | WebhookExistsException ex) {
            response.setStatus(ApiResponseStatus.FAILED);
            response.setMessage(ex.getMessage());
            response.setData(null);
            statusCode = HttpStatus.BAD_REQUEST;
        } catch (Exception ex) {
            response.setStatus(ApiResponseStatus.FAILED);
            response.setMessage("Webhook registration failed");
            response.setData(null);
            statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return ResponseEntity.status(statusCode).body(response);
    }

    @GetMapping(API_TRIGGERS)
    public ResponseEntity<ApiResponse<List<WebhookTrigger>>> getTriggersList() {
        ApiResponse<List<WebhookTrigger>> response = new ApiResponse<>();
        response.setData(List.of(WebhookTrigger.values()));
        response.setStatus(ApiResponseStatus.SUCCESS);
        return ResponseEntity.ok(response);
    }

    @GetMapping(API_LOGS)
    public ResponseEntity<ApiResponse<List<WebhookWithLog>>> getWebhookLogs(Principal principal) {
        String userId = principal.getName();
        ApiResponse<List<WebhookWithLog>> response = new ApiResponse<>();
        response.setData(webhookService.getWebhookLogs(userId));
        response.setStatus(ApiResponseStatus.SUCCESS);
        return ResponseEntity.ok(response);
    }
}
