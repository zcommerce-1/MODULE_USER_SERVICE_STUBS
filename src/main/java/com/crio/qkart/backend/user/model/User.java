package com.crio.qkart.backend.user.model;

import com.crio.qkart.backend.user.model.enums.UserType;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@AllArgsConstructor
public class User extends BaseModel {

  @NotNull
  @Email
  private String email;

  @NotNull
  private String fullName;

  @NotNull
  private UserType type;

  @NotNull
  private String address;
}


