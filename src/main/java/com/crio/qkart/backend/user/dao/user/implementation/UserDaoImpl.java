package com.crio.qkart.backend.user.dao.user.implementation;

import com.crio.qkart.backend.user.common.exceptions.EntityDoesNotExistException;
import com.crio.qkart.backend.user.dao.user.UserDao;
import com.crio.qkart.backend.user.dao.user.entity.UserEntity;
import com.crio.qkart.backend.user.dao.user.repository.UserRepository;
import com.crio.qkart.backend.user.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Log4j2
@RequiredArgsConstructor
public class UserDaoImpl implements UserDao {

  private final UserRepository userRepository;

  private final ModelMapper modelMapper;


  @Override
  public Optional<User> findByUserId(Long userId) {
    Optional<UserEntity> userEntityOptional = userRepository.findById(userId);

    // check if review details is present
    if (userEntityOptional.isPresent()) {
      User user = modelMapper.map(userEntityOptional.get(), User.class);
      return Optional.of(user);
    }

    // returning empty optional as cart details is not present
    return Optional.empty();
  }

  @Override
  public User addUser(User user) {
    UserEntity userEntity = modelMapper.map(user, UserEntity.class);

    // inserting review entity into user table
    UserEntity userEntity1 = userRepository.save(userEntity);
    return modelMapper.map(userEntity1, User.class);
  }

  @Override
  public User updateUser(Long userId, User user) throws EntityDoesNotExistException {
    String logPrefix = "event: updateUser, message: ";

    Optional<UserEntity> existingUserEntityOptional = userRepository.findById(userId);

    if (!existingUserEntityOptional.isPresent()) {
      log.info(logPrefix + "user details not found for the userId : {}", userId);
      throw new EntityDoesNotExistException(
          String.format("%s update failed as user details not found for the userId : %s",
              logPrefix, userId));
    }

    UserEntity userEntity = existingUserEntityOptional.get();
    // todo use mapper and a patch request object
    userEntity.setEmail(user.getEmail());
    userEntity.setType(user.getType());
    userEntity.setFullName(user.getFullName());
    userEntity.setAddress(user.getAddress());
    return modelMapper.map(userRepository.save(userEntity), User.class);
  }

  @Override
  public void deleteByUserId(Long userId) {
    userRepository.deleteById(userId);
  }

}
