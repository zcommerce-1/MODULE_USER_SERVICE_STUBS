package com.crio.qkart.backend.user.config.kafka;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;

@EnableKafka
@Configuration
@ConfigurationProperties(prefix = "kafka")
@Data
public class KafkaTopicConfig {
  @Value("${kafka.topic.clickstream.name}")
  private String clickStreamTopic;

  @Value("${kafka.topic.clickstream.consumer-group}")
  private String clickStreamConsumerGroup;

  private String bootstrapAddress;
}
