package com.crio.qkart.backend.user.utils.kafka.model;

import com.crio.qkart.backend.user.model.Activity;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ActivityMessageEvent implements ConsumerEvent {
    String userId;
    @NotNull private Activity activity;
}
