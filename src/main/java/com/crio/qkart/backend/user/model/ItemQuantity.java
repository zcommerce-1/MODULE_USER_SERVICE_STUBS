package com.crio.qkart.backend.user.model;

import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode
public class ItemQuantity implements Serializable {

  @NotNull
  private Long itemId;

  @NotNull
  private int quantity;
}
