package com.crio.qkart.backend.user.dao.clickstream.activity.entity;

import com.crio.qkart.backend.user.dao.BaseEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.time.ZonedDateTime;
import java.util.List;

@Getter
@Setter
@ToString
@SuperBuilder
@RequiredArgsConstructor
@Entity
@Table(
        name = "activity"
)
public class ActivityEntity extends BaseEntity {
    private String pageUrl;
    private String referer;
    private String userId;
    private String eventType;
    private String element_id;
    private String element_class;
    private ZonedDateTime eventTime;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "activity_id")
    private List<ActivityPropertyEntity> attributes;
}
