package com.crio.qkart.backend.user.service;

import com.crio.qkart.backend.user.common.exceptions.InvalidWebhookCallbackException;
import com.crio.qkart.backend.user.common.exceptions.WebhookExistsException;
import com.crio.qkart.backend.user.model.Webhook;
import com.crio.qkart.backend.user.model.WebhookWithLog;

import java.util.List;

public interface WebhookService {
    /**
     * Register a new webhook
     *
     * @param webhook of type Webhook. Details of the webhook request
     * @throws InvalidWebhookCallbackException if the callback url could not be verified
     * @throws WebhookExistsException if webhook with the same trigger for the given user exists
     */
    Webhook createWebhook(Webhook webhook)
            throws InvalidWebhookCallbackException, WebhookExistsException;

    List<WebhookWithLog> getWebhookLogs(String userId);
}
