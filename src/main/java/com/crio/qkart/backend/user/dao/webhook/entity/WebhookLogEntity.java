package com.crio.qkart.backend.user.dao.webhook.entity;

import com.crio.qkart.backend.user.dao.BaseEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.time.ZonedDateTime;

@Getter
@Setter
@ToString
@SuperBuilder
@RequiredArgsConstructor
@Entity
@Table(
        name = "webhook_events",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "id")
        }
)
public class WebhookLogEntity extends BaseEntity {
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "wh_id")
    private WebhookEntity webhook;
    @Column(name = "wh_trigger")
    private String trigger;
    private ZonedDateTime sentAt;
    private int statusCode;
    @Column(name = "wh_comment")
    private String comment;
}
