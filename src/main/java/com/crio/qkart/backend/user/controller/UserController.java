package com.crio.qkart.backend.user.controller;


import com.crio.qkart.backend.user.common.api.ApiResponse;
import com.crio.qkart.backend.user.common.api.ApiResponseStatus;
import com.crio.qkart.backend.user.common.exceptions.EntityDoesNotExistException;
import com.crio.qkart.backend.user.model.Activity;
import com.crio.qkart.backend.user.model.User;
import com.crio.qkart.backend.user.service.UserActivityService;
import com.crio.qkart.backend.user.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.metrics.annotation.Timed;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Slf4j
@Timed
@RestController
@RequiredArgsConstructor
@RequestMapping(UserController.USER_API_PREFIX)
public class UserController {

  public static final String USER_API_PREFIX = "api/v1/user";
  private static final String ID_ENDPOINT = "/{id}";
  private static final String ACTIVITY_ASYNC_ENDPOINT = "/activityAsync";
  private static final String ACTIVITY_SYNC_ENDPOINT = "/activitySync";

  private final UserService userService;
  private final UserActivityService userActivityService;


  /**
   * URL: /api/v1/user
   * Description: Get user details.
   * Method: GET
   *
   * @return user details
   */
  @GetMapping(ID_ENDPOINT)
  @Timed
  public ResponseEntity<ApiResponse<User>> getUser(@PathVariable("id") Long userId) {
    ApiResponse<User> response = new ApiResponse<>();

    try {
      User user = userService.getUserDetails(userId);
      response.setData(user);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(e.getMessage());
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
  }

  /**
   * URL: /api/v1/user
   * Description: Add user
   * Method: POST
   *
   * @param user of type User
   * @return Created user details
   */
  @PostMapping
  @Timed
  public ResponseEntity<ApiResponse<User>> addCategory(@RequestBody @Valid User user) {
    // Creating new user
    User addedUser = userService.addUser(user);

    ApiResponse<User> response = new ApiResponse<>();
    response.setData(addedUser);
    response.setStatus(ApiResponseStatus.SUCCESS);
    return ResponseEntity.status(HttpStatus.OK).body(response);
  }

  /**
   * URL: /api/v1/user/{id}
   * Description: Update user details
   * Method: PUT
   *
   * @param user of type User
   * @return updated user details
   */
  @PutMapping(ID_ENDPOINT)
  @Timed
  public ResponseEntity<ApiResponse<User>> updateUser(@PathVariable("id") Long userId,
      @RequestBody @Valid User user) {
    ApiResponse<User> response = new ApiResponse<>();
    try {
      User updateUser = userService.updateUser(userId, user);
      response.setData(updateUser);
      response.setStatus(ApiResponseStatus.SUCCESS);
      return ResponseEntity.status(HttpStatus.OK).body(response);
    } catch (EntityDoesNotExistException e) {
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage(String.format(e.getMessage()));
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
  }

  /**
   * URL: /api/v1/user/{id}
   * Description: Delete user of given user id
   * Method: DELETE
   */
  @DeleteMapping(ID_ENDPOINT)
  @Timed
  public ResponseEntity<ApiResponse<String>> deleteUser(@PathVariable("id") Long userId) {
    userService.deleteUser(userId);
    ApiResponse<String> response = new ApiResponse<>();
    response.setStatus(ApiResponseStatus.SUCCESS);
    response.setMessage("User deleted");
    return ResponseEntity.status(HttpStatus.OK).body(response);
  }

  /**
   * URL: /api/v1/user/activitySync
   * Description: Add user activities (clickstream data) synchronously
   * Method: POST
   */
  @PostMapping(ACTIVITY_SYNC_ENDPOINT)
  @Timed
  public ResponseEntity<ApiResponse<String>> addActivitySync(
          @RequestBody @Valid Activity activity,
          Principal principal) {
    String userId = principal.getName();
    ApiResponse<String> response = new ApiResponse<>();
    HttpStatus status = HttpStatus.CREATED;

    try {
      userActivityService.addActivitySync(activity, userId);

      response.setStatus(ApiResponseStatus.SUCCESS);
      response.setMessage("OK");
    } catch (InvalidDataAccessResourceUsageException ex) {
      log.error("exception occurred when trying to save activity details sync", ex);
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage("Failed to save user activity - Too many requests");
      status = HttpStatus.TOO_MANY_REQUESTS;
    }
    catch (Exception ex) {
      log.error("exception occurred when trying to save activity details sync", ex);
      response.setStatus(ApiResponseStatus.FAILED);
      response.setMessage("Failed to save user activity");
      status = HttpStatus.INTERNAL_SERVER_ERROR;
    }
    return ResponseEntity.status(status).body(response);
  }

  /**
   * URL: /api/v1/user/activityAsync
   * Description: Add user activities (clickstream data) synchronously
   * Method: POST
   */
  @PostMapping(ACTIVITY_ASYNC_ENDPOINT)
  @Timed
  public ResponseEntity<ApiResponse<String>> addActivityAsync(
          @RequestBody @Valid Activity activity,
          Principal principal) {
    String userId = principal.getName();
    userActivityService.addActivityAsync(activity, userId);

    ApiResponse<String> response = new ApiResponse<>();
    response.setStatus(ApiResponseStatus.SUCCESS);
    response.setMessage("OK");
    return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
  }
}
